package com.example.sample;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.TextView;

import java.util.ArrayList;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static String DBNAME = "studentManager.db";

    private static String SQLDROPDB = "DROP TABLE IF EXISTS students";
    private static String STUDENTS_TABLE = "students";
    private static String STUDENT_NO = "student_no";
    private static String NAME = "name";
    private static String PRELIM = "prelim";
    private static String MIDTERM = "midterm";
    private static String FINALS = "finals";
    private String GET_ALL_STUDENTS;


    public DatabaseHandler(Context context) {
        super(context, DBNAME, null, 1);
        GET_ALL_STUDENTS = "SELECT * FROM " + STUDENTS_TABLE;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQLCREATEDB = "CREATE TABLE ";
        SQLCREATEDB += STUDENTS_TABLE;
        SQLCREATEDB += " (id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        SQLCREATEDB += STUDENT_NO + " TEXT NOT NULL UNIQUE, ";
        SQLCREATEDB += NAME + " TEXT NOT NULL, ";
        SQLCREATEDB += PRELIM + " TEXT, ";
        SQLCREATEDB += MIDTERM + " TEXT, ";
        SQLCREATEDB += FINALS + " TEXT)";
        db.execSQL(SQLCREATEDB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQLDROPDB);
        onCreate(db);
    }

    public boolean addStudent(String student_no, String name, String prelim, String midterm, String finals) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(STUDENT_NO, student_no);
        contentValues.put(NAME, name);
        contentValues.put(PRELIM, prelim);
        contentValues.put(MIDTERM, midterm);
        contentValues.put(FINALS, finals);
        db.insert(STUDENTS_TABLE, null, contentValues);
        return true;
    }

    public String viewAllStudent() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor students = db.rawQuery(GET_ALL_STUDENTS, null);

        String records = "";

        if (students.moveToFirst()) {
            do {
                records += "STUDENT NO: " + students.getString(students.getColumnIndex(STUDENT_NO));
                records += ", NAME: " + students.getString(students.getColumnIndex(NAME));
                records += ", PRELIM " + students.getString(students.getColumnIndex(PRELIM));
                records += ", MIDTERM: " + students.getString(students.getColumnIndex(MIDTERM));
                records += ", FINALS: " + students.getString(students.getColumnIndex(FINALS)) + "\n";
            } while (students.moveToNext());
        }

        return records;
    }

    public Cursor get(Integer id) {
        String GET_STUDENT_BY_ID = "SELECT * FROM " + STUDENTS_TABLE + " WHERE id=" + Integer.toString(id);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor student = db.rawQuery(GET_STUDENT_BY_ID, null);
        return student;
    }

    public boolean updateStudent(Integer id, String student_no, String name, String prelim, String midterm, String finals) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(STUDENT_NO, student_no);
        contentValues.put(NAME, name);
        contentValues.put(PRELIM, prelim);
        contentValues.put(MIDTERM, midterm);
        contentValues.put(FINALS, finals);
        db.update(STUDENTS_TABLE, contentValues, "id = ? ", new String[] { Integer.toString(id)});
        return true;
    }

    public Integer deleteStudent(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(STUDENTS_TABLE, "id = ?", new String[] { Integer.toString(id)});
    }

    public ArrayList<String> allStudent() {
        ArrayList<String> students = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(GET_ALL_STUDENTS, null);

        if (cursor.moveToFirst()){
            do {
                students.add(cursor.getString(cursor.getColumnIndex(NAME)));
            } while (cursor.moveToNext());
        }
        return students;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getWritableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, STUDENTS_TABLE);
        return numRows;
    }

}
