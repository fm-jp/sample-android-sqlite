package com.example.sample;

import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText student_no, name, prelim, midterm, finals;
    TextView textView;
    Button btnSave, btnViewAll;
    DatabaseHandler dbHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHandler = new DatabaseHandler(this);

        student_no = (EditText)findViewById(R.id.student_no);
        name = (EditText)findViewById(R.id.name);
        prelim = (EditText)findViewById(R.id.prelim);
        midterm = (EditText)findViewById(R.id.midterm);
        finals = (EditText)findViewById(R.id.finals);

        textView = (TextView)findViewById(R.id.view);

        btnSave = (Button)findViewById(R.id.btnSave);
        btnViewAll = (Button)findViewById(R.id.btnViewAll);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    boolean saved = dbHandler.addStudent(student_no.getText().toString(),
                            name.getText().toString(),
                            prelim.getText().toString(),
                            midterm.getText().toString(),
                            finals.getText().toString()
                    );
                    if (saved) {
                        Toast.makeText(MainActivity.this, "Record saved!", Toast.LENGTH_SHORT).show();
                        student_no.setText("");
                        name.setText("");
                        prelim.setText("");
                        midterm.setText("");
                        finals.setText("");

                        showRecord();
                        student_no.requestFocus();
                    }
                } catch (SQLiteException e) {
                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRecord();
            }
        });


    }

    public void showRecord() {
        String records = dbHandler.viewAllStudent();
        textView.setText(records);
    }


}
